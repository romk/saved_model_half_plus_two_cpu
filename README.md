Example TensorFlow model [saved_model_half_plus_two_cpu](https://github.com/tensorflow/serving/tree/master/tensorflow_serving/servables/tensorflow/testdata/saved_model_half_plus_two_cpu) embedded into a serving image.

## Usage

Start a server:
```
$ docker run -it -p 8501:8501 romk/saved_model_half_plus_two_cpu
```

Make an inference request:
```
$ curl localhost:8501/v1/models/saved_model_half_plus_two_cpu:predict -d '{"instances": [1.0, 2.0]}'
{
    "predictions": [2.5, 3.0]
}
```
